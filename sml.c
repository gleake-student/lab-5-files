#include <stdio.h> 
#include <stdlib.h>

int main(){
    
    
    FILE *startFile, *small, *medium, *large;
    startFile = fopen("homelistings.csv", "r");
    small = fopen("smallhomes.txt", "w");
    medium = fopen("mediumhomes.txt", "w");
    large = fopen("largehomes.txt", "w");
    
    if (!startFile){
        printf("Couldn't open startFile\n");
        exit(1);
    }
    
    
    long priceTotal=0;
    //int zipcode, houseID, housePrice, bedrooms, baths, squareFoot, lowPrice, highPrice, avg, count = 0;
    int zipcode, houseID, housePrice, bedrooms, baths, squareFoot;
    char address[50];
    
     
    while (fscanf(startFile, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &houseID, address, &housePrice, &bedrooms, &baths, &squareFoot) != EOF) {
        if (squareFoot < 1000) {
            fprintf(small, "%s : %d\n", address, squareFoot); 
        } 
        else if (squareFoot >= 1000 && squareFoot <= 2000) {
            fprintf(medium, "%s : %d\n", address, squareFoot);    
        }
        else {
            fprintf(large, "%s : %d\n", address, squareFoot);
        }
        
    }
    
    fclose(startFile);
}