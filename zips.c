#include <stdio.h> 
#include <stdlib.h>

int main(){
    
    
    FILE *startFile;
    startFile = fopen("homelistings.csv", "r");
    
    
    if (!startFile){
        printf("Couldn't open startFile\n");
        exit(1);
    }
    
    
    long priceTotal=0;
    
    int zipcode, houseID, housePrice, bedrooms, baths, squareFoot;
    char address[50];
    
     
    while (fscanf(startFile, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &houseID, address, &housePrice, &bedrooms, &baths, &squareFoot) != EOF) {
        char filename[10];
        sprintf(filename, "%d.text", zipcode);
        FILE *zipCodes = fopen(filename, "a");
        if (!zipCodes) {
            printf("Can't open file: %s\n", filename);
        }
        fprintf(zipCodes, "%s\n", address);
        fclose(zipCodes);
        
    }
    
    fclose(startFile);
}

