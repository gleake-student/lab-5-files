#include <stdio.h> 
#include <stdlib.h>

int main(){
    
    FILE *h;
    h=fopen("homelistings.csv", "r");

    if (!h){
        printf("Couldn't open homelistings.csv\n");
        exit(1);
    }
    
    long priceTotal=0;
    int zipcode, houseID, housePrice, bedrooms, baths, squareFoot, lowPrice, highPrice, avg, count = 0;
    char address[50];
    
    while (fscanf(h, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &houseID, address, &housePrice, &bedrooms, &baths, &squareFoot) != EOF) {
        if (count == 0) {
            lowPrice = housePrice;
            highPrice = housePrice;
        } else {
            if (housePrice < lowPrice) {
                lowPrice = housePrice;
            } else if (highPrice > lowPrice) {
                highPrice = housePrice;
            }
        }
        priceTotal += housePrice;
        count++;
    }
    avg = priceTotal / count;
    printf("%d %d %d\n", lowPrice, highPrice, avg);
    fclose(h);
    
    
    
}
